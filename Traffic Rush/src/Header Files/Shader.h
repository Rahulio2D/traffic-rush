#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <string>
#include <map>

enum class ShaderTypes
{
	FRAGMENT, VERTEX
};

class Shader
{
public:
	Shader();
	~Shader();

	void Bind() const;
	void Unbind() const;

	void CreateShader(ShaderTypes shaderType, std::string shaderPath);

	void SetMat4(std::string uniformName, int count, bool transpose, glm::mat4 data);
	void SetVec3(std::string uniformName, glm::vec3 vec);
	void SetVec3(std::string uniformName, float x, float y, float z);
	void SetFloat(std::string uniformName, float value);
	void SetInt(std::string uniformName, int value);

	std::map<std::string, unsigned int> uniformCache;
	unsigned int GetShaderID() { return shaderID; }

private:
	unsigned int shaderID;

	unsigned int ReadShaderFromFile(std::string shaderPath, std::string &shaderSource);
	unsigned int CompileShader(ShaderTypes shaderType, std::string shaderSource);
	unsigned int GetUniformLocation(std::string uniformName);
	unsigned int GetShaderType(ShaderTypes shaderType);
};