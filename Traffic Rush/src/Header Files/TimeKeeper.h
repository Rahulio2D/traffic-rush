#pragma once

class TimeKeeper
{
public:
	void CalculateDeltaTime();
	static float GetDeltaTime() { return deltaTime; }

private:
	static float deltaTime;

	static float timeThisFrame;
	static float timePreviousFrame;
};