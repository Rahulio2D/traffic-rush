#pragma once

#include <vector>

class Texture
{
public:
	Texture();
	~Texture();

	unsigned int LoadTexture(const char* texturePath);
	void Bind(unsigned int textureOffset) const;
	void Unbind() const;

private:
	std::vector<unsigned int> textureIDs;
};