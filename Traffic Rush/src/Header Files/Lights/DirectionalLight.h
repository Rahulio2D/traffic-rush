#pragma once

#include <glm/gtc/matrix_transform.hpp>

struct DirectionalLight {
	glm::vec3 direction = glm::vec3(0, -1, 0);
	glm::vec3 ambient = glm::vec3(0.2f);
	glm::vec3 diffuse = glm::vec3(0.8f);
	glm::vec3 specular = glm::vec3(1.0f);
};