#pragma once

#include <glm/gtc/matrix_transform.hpp>

struct SpotLight {
	glm::vec3 position = glm::vec3(0.0f);
	glm::vec3 direction = glm::vec3(0.0f);
	glm::vec3 ambient = glm::vec3(0.2f);
	glm::vec3 diffuse = glm::vec3(0.8f);
	glm::vec3 specular = glm::vec3(1.0f);

	float innerRadius = glm::cos(glm::radians(10.0f));
	float outerRadius = glm::cos(glm::radians(20.0f));

	float constant = 1.0f;
	float linear = 0.09f;
	float quadratic = 0.032f;
};