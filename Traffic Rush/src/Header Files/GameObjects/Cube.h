#pragma once

#include <glm/gtc/matrix_transform.hpp>
class VertexArray;
class VertexBuffer;
class VertexBufferLayout;
class IndexBuffer;
class Shader;
struct Material;

class Cube
{
public:
	Cube();
	~Cube();

	void Update();
	void Render();

private:
	VertexArray* vertexArray;
	VertexBuffer* vertexBuffer;
	VertexBufferLayout* vertexBufferLayout;
	IndexBuffer* indexBuffer;
	Shader* shader;
	Material* material;
	unsigned int indexCount;

	glm::mat4 transformationMatrix;
};