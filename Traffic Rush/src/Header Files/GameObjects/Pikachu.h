#pragma once

#include <glm/gtc/matrix_transform.hpp>
class VertexArray;
class VertexBuffer;
class VertexBufferLayout;
class IndexBuffer;
class Shader;
class Texture;

class Pikachu
{
public:
	Pikachu();
	~Pikachu();

	void Update();
	void Render();

	Shader* GetShader() { return shader; }

private:
	VertexArray* vertexArray;
	VertexBuffer* vertexBuffer;
	VertexBufferLayout* vertexBufferLayout;
	IndexBuffer* indexBuffer;
	Shader* shader;
	Texture* texture;

	glm::mat4 transformationMatrix;
};