#pragma once

#include <glm/gtc/matrix_transform.hpp>
class VertexArray;
class VertexBuffer;
class VertexBufferLayout;
class IndexBuffer;
class Shader;
class Texture;
struct Material;

class TexturedCube
{
public:
	TexturedCube();
	~TexturedCube();

	void Update();
	void Render();

private:
	VertexArray* vertexArray;
	VertexBuffer* vertexBuffer;
	VertexBufferLayout* vertexBufferLayout;
	IndexBuffer* indexBuffer;
	Shader* shader;
	Material* material;
	Texture* texture;
	unsigned int indexCount;

	glm::mat4 transformationMatrix;
};