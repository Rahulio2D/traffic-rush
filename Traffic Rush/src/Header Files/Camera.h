#pragma once

#include <glm/gtc/matrix_transform.hpp>

class Camera
{
public:
	Camera(glm::vec3 cameraPosition, float cameraFOV, float nearPlane, float farPlane);

	void Update();

	static glm::mat4 VIEW_MATRIX;
	static glm::mat4 PROJECTION_MATRIX;
	static glm::vec3 POSITION;
};