#pragma once

#include <vector>

struct VertexLayoutElement
{
	unsigned int type;
	unsigned int count;
	unsigned char normalised;
};

class VertexBufferLayout
{
public:
	VertexBufferLayout();
	~VertexBufferLayout();

	template <typename T>
	void Push(unsigned int count);

	inline const std::vector<VertexLayoutElement> GetElements() const& { return elements; }
	inline unsigned int GetStride() const { return stride; }

private:
	std::vector<VertexLayoutElement> elements;
	unsigned int stride;
};