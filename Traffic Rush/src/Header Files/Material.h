#pragma once

#include <glm/gtc/matrix_transform.hpp>

struct Material {
	glm::vec3 diffuse = glm::vec3(1.0f);
	glm::vec3 specular = glm::vec3(1.0f);
	float shininess = 32.0f;
};