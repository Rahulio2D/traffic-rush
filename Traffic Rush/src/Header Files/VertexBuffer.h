#pragma once

class VertexBuffer
{
public:
	VertexBuffer(void* data, unsigned int size);
	~VertexBuffer();

	void Bind();
	void Unbind();

private:
	unsigned int bufferID = 0;
};
