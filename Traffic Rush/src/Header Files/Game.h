#pragma once

class GLFWwindow;
class Pikachu;
class Camera;
class Cube;
class TexturedCube;
class LightManager;

class Game
{
public:
  Game();
  ~Game();
  void Initialise(unsigned int width, unsigned int height, const char* title);
  void Update();
  void HandleInput();
  void Render();

  bool IsRunning() { return isRunning; }

  static int WIDTH;
  static int HEIGHT;

private:
  GLFWwindow* window;
  Camera* camera;
  Pikachu* pikachu;
  Cube* cube;
  TexturedCube* texturedCube;
  LightManager* lightManager;
  bool isRunning;
};