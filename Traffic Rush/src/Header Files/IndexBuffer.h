#pragma once

class IndexBuffer
{
public:
	IndexBuffer(unsigned int* data, unsigned int count);
	~IndexBuffer();

	void Bind();
	void Unbind();

private:
	unsigned int bufferID = 0;
};
