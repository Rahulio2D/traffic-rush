#pragma once

#include "Lights/DirectionalLight.h"
#include "Lights/PointLight.h"
#include "Lights/SpotLight.h"
#include <vector>

class LightManager
{
public:
	static unsigned int SPOTLIGHTS_COUNT() { return SPOT_LIGHTS.size(); }
	static SpotLight* GetSpotLight(unsigned int n) { return SPOT_LIGHTS[n]; }
	unsigned int AddSpotLight(SpotLight* spotLight);

	static unsigned int POINTLIGHTS_COUNT() { return POINT_LIGHTS.size(); }
	static PointLight* GetPointLight(unsigned int n) { return POINT_LIGHTS[n]; }
	unsigned int AddPointLight(PointLight* pointLight);

	static DirectionalLight* DIRECTIONAL_LIGHT;

private:
	static std::vector<SpotLight*> SPOT_LIGHTS;
	static std::vector<PointLight*> POINT_LIGHTS;
};