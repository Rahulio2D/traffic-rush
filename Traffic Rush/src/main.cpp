#include "Game.h"
#include "TimeKeeper.h"
#include <iostream>

int main(void)
{
  Game* game = new Game();
  TimeKeeper* time = new TimeKeeper;
  game->Initialise(800, 800, "Traffic Rush");

  while(game->IsRunning())
  {
    game->Update();
    game->HandleInput();
    game->Render();
    time->CalculateDeltaTime();
  }
  delete time;
  delete game;
  return 0;
}