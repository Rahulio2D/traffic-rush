#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include <GL/glew.h>

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &vertexArrayID);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &vertexArrayID);
}

void VertexArray::AddBuffer(VertexBuffer& buffer, const VertexBufferLayout& layout)
{
	this->Bind();
	buffer.Bind();
	std::vector<VertexLayoutElement> elements = layout.GetElements();
	int offset = 0;
	for(int i = 0; i < elements.size(); i++)
	{
		VertexLayoutElement element = elements[i];
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, element.count, element.type, element.normalised, layout.GetStride(), (const void*)offset);
		offset += element.count * sizeof(element.type);
	}
}

void VertexArray::Bind() const
{
	glBindVertexArray(vertexArrayID);
}

void VertexArray::Unbind() const
{
	glBindVertexArray(0);
}