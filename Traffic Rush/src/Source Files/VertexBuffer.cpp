#include "VertexBuffer.h"
#include <GL/glew.h>

/// <summary>
/// Initialise a new Vertex Buffer
/// </summary>
/// <param name="data"> The data of the vertices </param>
/// <param name="size"> The size (in bytes) of the data </param>
VertexBuffer::VertexBuffer(void* data, unsigned int size)
{
  glGenBuffers(1, &bufferID);
  glBindBuffer(GL_ARRAY_BUFFER, bufferID);
  glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}

VertexBuffer::~VertexBuffer()
{
  glDeleteBuffers(1, &bufferID);
}

void VertexBuffer::Bind()
{
  glBindBuffer(GL_ARRAY_BUFFER, bufferID);
}

void VertexBuffer::Unbind()
{
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}
