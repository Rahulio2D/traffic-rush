#include "LightManager.h"
#include <iostream>

DirectionalLight* LightManager::DIRECTIONAL_LIGHT = new DirectionalLight();
std::vector<SpotLight*> LightManager::SPOT_LIGHTS = std::vector<SpotLight*>();
std::vector<PointLight*> LightManager::POINT_LIGHTS = std::vector<PointLight*>();

unsigned int LightManager::AddSpotLight(SpotLight* spotLight)
{
	if (SPOT_LIGHTS.size() < 4)
	{
		SPOT_LIGHTS.push_back(spotLight);
		return 0;
	}
	std::cout << "ERROR: Can not have more than four Spot Lights!\n";
	return -1;
}

unsigned int LightManager::AddPointLight(PointLight* pointLight)
{
	if (POINT_LIGHTS.size() < 4)
	{
		POINT_LIGHTS.push_back(pointLight);
		return 0;
	}
	std::cout << "ERROR: Can not have more than four Point Lights!\n";
	return -1;
}
