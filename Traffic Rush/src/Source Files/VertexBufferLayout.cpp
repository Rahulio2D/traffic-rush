#include "VertexBufferLayout.h"
#include <GL/glew.h>

VertexBufferLayout::VertexBufferLayout()
{
	stride = 0;
	elements = std::vector<VertexLayoutElement>();
}

VertexBufferLayout::~VertexBufferLayout()
{
	elements.clear();
}

template<>
void VertexBufferLayout::Push<float>(unsigned int count)
{
	elements.push_back({ GL_FLOAT, count, GL_FALSE });
	stride += sizeof(GLfloat) * count;
}

template<>
void VertexBufferLayout::Push<unsigned int>(unsigned int count)
{
	elements.push_back({ GL_UNSIGNED_INT, count, GL_FALSE });
	stride += sizeof(GLuint) * count;
}

template<>
void VertexBufferLayout::Push<unsigned char>(unsigned int count)
{
	elements.push_back({ GL_UNSIGNED_BYTE, count, GL_TRUE });
	stride += sizeof(GLubyte) * count;
}
