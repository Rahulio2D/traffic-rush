#include "IndexBuffer.h"
#include <GL/glew.h>

/// <summary>
/// Initialise a new Index Buffer
/// </summary>
/// <param name="indices"> The indices relative to Vertex Buffer </param>
/// <param name="count"> The number of indices </param>
IndexBuffer::IndexBuffer(unsigned int* indices, unsigned int count)
{
  glGenBuffers(1, &bufferID);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferID);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), indices, GL_STATIC_DRAW);
}

IndexBuffer::~IndexBuffer()
{
  glDeleteBuffers(1, &bufferID);
}

void IndexBuffer::Bind()
{
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferID);
}

void IndexBuffer::Unbind()
{
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
