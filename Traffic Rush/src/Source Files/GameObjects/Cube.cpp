#include "GameObjects/Cube.h"
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "Shader.h"
#include "Camera.h"
#include "Material.h"
#include "LightManager.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

Cube::Cube()
{
  float vertices[] = {
    -1.0f,  1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f,
     1.0f,  1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f,
    -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f,
     1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f,
                               
     1.0f,  1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
     1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
     1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
     1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
                               
    -1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
     1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
    -1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
     1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                               
    -1.0f,  1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f,
    -1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f,
    -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f,
    -1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f,
                               
    -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f,
     1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f,
    -1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f,
     1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f,
                               
    -1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
     1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
    -1.0f,  1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
     1.0f,  1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f
  };

  unsigned int indices[] = {
    3, 2, 1,
    1, 2, 0,

    7, 6, 5,
    5, 6, 4,

    8, 10, 9,
    9, 10, 11,

    12, 14, 13,
    13, 14, 15,

    19, 18, 17,
    17, 18, 16,

    23, 22, 21,
    21, 22, 20
  };

  indexCount = sizeof(indices) / sizeof(unsigned int);

  vertexArray = new VertexArray();
  vertexBuffer = new VertexBuffer(vertices, sizeof(vertices));
  vertexBufferLayout = new VertexBufferLayout();
  vertexBufferLayout->Push<float>(3);
  vertexBufferLayout->Push<float>(3);
  vertexBufferLayout->Push<float>(3);
  vertexArray->AddBuffer(*vertexBuffer, *vertexBufferLayout);
  indexBuffer = new IndexBuffer(indices, indexCount);

  shader = new Shader();
  shader->CreateShader(ShaderTypes::FRAGMENT, "Shaders/BasicObject.fragment");
  shader->CreateShader(ShaderTypes::VERTEX, "Shaders/BasicObject.vertex");

  vertexArray->Unbind();
  vertexBuffer->Unbind();
  indexBuffer->Unbind();
  shader->Unbind();

  material = new Material;
  material->diffuse = glm::vec3(0.5f);
  material->specular = glm::vec3(1.0f, 1.0f, 1.0f);
  material->shininess = 32.0f;

  transformationMatrix = glm::mat4(1.0f);
  transformationMatrix = glm::translate(transformationMatrix, glm::vec3(0.0f));
  transformationMatrix = glm::scale(transformationMatrix, glm::vec3(1.2f, 1.2f, 1.2f));
  transformationMatrix = glm::rotate(transformationMatrix, 45.0f, glm::vec3(1.0f, 0.3f, 0.5f));
}

Cube::~Cube()
{
  delete vertexArray;
  delete vertexBuffer;
  delete indexBuffer;
  delete shader;
  delete material;
}

void Cube::Update()
{
  transformationMatrix = glm::rotate(transformationMatrix, (float)glfwGetTime() * 0.001f, glm::vec3(1.0f, 1.0f, 1.0f));
}

void Cube::Render()
{
  vertexArray->Bind();
  shader->Bind();

  shader->SetMat4("modelMatrix", 1, false, transformationMatrix);
  shader->SetMat4("viewMatrix", 1, false, Camera::VIEW_MATRIX);
  shader->SetMat4("projectionMatrix", 1, false, Camera::PROJECTION_MATRIX);

  shader->SetVec3("cameraPosition", Camera::POSITION);

  shader->SetVec3("material.specular", material->specular);
  shader->SetVec3("material.diffuse", material->diffuse);
  shader->SetFloat("material.shininess", material->shininess);

  shader->SetVec3("directionalLight.direction", LightManager::DIRECTIONAL_LIGHT->direction);
  shader->SetVec3("directionalLight.ambient", LightManager::DIRECTIONAL_LIGHT->ambient);
  shader->SetVec3("directionalLight.diffuse", LightManager::DIRECTIONAL_LIGHT->diffuse);
  shader->SetVec3("directionalLight.specular",LightManager::DIRECTIONAL_LIGHT->specular);

  int numPointLights = LightManager::POINTLIGHTS_COUNT();
  shader->SetInt("numberOfPointLights", numPointLights);
  for (int i = 0; i < numPointLights; i++)
  {
    std::string baseString = "pointLights[" + std::to_string(i) + "].";
    shader->SetVec3(baseString + "position", LightManager::GetPointLight(i)->position);
    shader->SetVec3(baseString + "ambient", LightManager::GetPointLight(i)->ambient);
    shader->SetVec3(baseString + "diffuse", LightManager::GetPointLight(i)->diffuse);
    shader->SetVec3(baseString + "specular", LightManager::GetPointLight(i)->specular);
    shader->SetFloat(baseString + "constant", LightManager::GetPointLight(i)->constant);
    shader->SetFloat(baseString + "linear", LightManager::GetPointLight(i)->linear);
    shader->SetFloat(baseString + "quadratic", LightManager::GetPointLight(i)->quadratic);
  }

  int numSpotLights = LightManager::SPOTLIGHTS_COUNT();
  shader->SetInt("numberOfSpotLights", numSpotLights);
  for (int i = 0; i < numSpotLights; i++)
  {
    std::string baseString = "spotLights[" + std::to_string(i) + "].";
    shader->SetVec3(baseString + "position", LightManager::GetSpotLight(i)->position);
    shader->SetVec3(baseString + "direction", LightManager::GetSpotLight(i)->direction);
    shader->SetVec3(baseString + "ambient", LightManager::GetSpotLight(i)->ambient);
    shader->SetVec3(baseString + "diffuse", LightManager::GetSpotLight(i)->diffuse);
    shader->SetVec3(baseString + "specular", LightManager::GetSpotLight(i)->specular);
    shader->SetFloat(baseString + "constant", LightManager::GetSpotLight(i)->constant);
    shader->SetFloat(baseString + "linear", LightManager::GetSpotLight(i)->linear);
    shader->SetFloat(baseString + "quadratic", LightManager::GetSpotLight(i)->quadratic);
    shader->SetFloat(baseString + "innerRadius", LightManager::GetSpotLight(i)->innerRadius);
    shader->SetFloat(baseString + "outerRadius", LightManager::GetSpotLight(i)->outerRadius);
  }

  glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, nullptr);

  vertexArray->Unbind();
  shader->Unbind();
}
