#include "GameObjects/Pikachu.h"
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "Shader.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/type_ptr.hpp>
#include "Camera.h"
#include "stb_image.h"
#include <iostream>
#include "Texture.h"

Pikachu::Pikachu()
{
  float vertices[] = {
     -1.0f,  0.75f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
      1.0f,  0.75f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
     -1.0f, -0.75f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f,
      1.0f, -0.75f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f
  };

  unsigned int indices[] = {
    2, 0, 1,
    3, 2, 1
  };

  vertexArray = new VertexArray();
  vertexBuffer = new VertexBuffer(vertices, sizeof(vertices));
  vertexBufferLayout = new VertexBufferLayout();
  vertexBufferLayout->Push<float>(3);
  vertexBufferLayout->Push<float>(3);
  vertexBufferLayout->Push<float>(2);
  vertexArray->AddBuffer(*vertexBuffer, *vertexBufferLayout);
  indexBuffer = new IndexBuffer(indices, 6);
  texture = new Texture();
  texture->LoadTexture("Assets/Pikachu.jpg");

  shader = new Shader();
  shader->CreateShader(ShaderTypes::FRAGMENT, "Shaders/BasicTextured.fragment");
  shader->CreateShader(ShaderTypes::VERTEX, "Shaders/BasicTextured.vertex");

  vertexArray->Unbind();
  vertexBuffer->Unbind();
  indexBuffer->Unbind();
  shader->Unbind();
  texture->Unbind();

  transformationMatrix = glm::mat4(1.0f);
}

Pikachu::~Pikachu()
{
  delete vertexArray;
  delete vertexBuffer;
  delete indexBuffer;
  delete shader;
  delete texture;
}

void Pikachu::Update()
{
  transformationMatrix = glm::mat4(1.0f);
  transformationMatrix = glm::translate(transformationMatrix, glm::vec3(0, 0, 0));
  transformationMatrix = glm::rotate(transformationMatrix, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
  transformationMatrix = glm::scale(transformationMatrix, glm::vec3(3.0f, 3.0f, 3.0f));
}

void Pikachu::Render()
{
  vertexArray->Bind();
  shader->Bind();
  texture->Bind(0);

  shader->SetMat4("modelMatrix", 1, GL_FALSE, transformationMatrix);
  shader->SetMat4("viewMatrix", 1, GL_FALSE, Camera::VIEW_MATRIX);
  shader->SetMat4("projectionMatrix", 1, GL_FALSE, Camera::PROJECTION_MATRIX);

  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

  vertexArray->Unbind();
  shader->Unbind();
  texture->Unbind();
}
