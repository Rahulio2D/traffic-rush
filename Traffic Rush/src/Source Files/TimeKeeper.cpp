#include "TimeKeeper.h"
#include <GLFW/glfw3.h>

float TimeKeeper::deltaTime = 0;
float TimeKeeper::timeThisFrame = 0;
float TimeKeeper::timePreviousFrame = 0;

void TimeKeeper::CalculateDeltaTime()
{
	timePreviousFrame = timeThisFrame;
	timeThisFrame = glfwGetTime();

	deltaTime = timeThisFrame - timePreviousFrame;
}
