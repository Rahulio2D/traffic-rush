#include "Camera.h"
#include "Game.h"
#include <GLFW/glfw3.h>

glm::mat4 Camera::VIEW_MATRIX;
glm::mat4 Camera::PROJECTION_MATRIX;
glm::vec3 Camera::POSITION;

Camera::Camera(glm::vec3 position, float fieldOfView, float nearPlane, float farPlane)
{
	POSITION = position;

	glm::vec3 cameraTarget = glm::vec3(0.0f);
	glm::vec3 cameraDirection = glm::normalize(POSITION - cameraTarget);

	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));

	glm::vec3 cameraUp = glm::cross(cameraDirection, cameraRight);
	VIEW_MATRIX = glm::lookAt(POSITION, cameraTarget, cameraUp);
	PROJECTION_MATRIX = glm::perspective(glm::radians(fieldOfView), (float)Game::WIDTH / (float)Game::HEIGHT, nearPlane, farPlane);
}

void Camera::Update()
{
	const float radius = 10.0f;
	float camX = sin(glfwGetTime()) * radius;
	float camZ = cos(glfwGetTime()) * radius;

	POSITION = glm::vec3(camX, POSITION.y, camZ);

	VIEW_MATRIX = glm::lookAt(POSITION, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
}