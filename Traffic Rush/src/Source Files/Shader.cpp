#include "Shader.h"
#include <GL/glew.h>
#include <malloc.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <glm/gtc/type_ptr.hpp>

Shader::Shader()
{
  shaderID = glCreateProgram();
  uniformCache = std::map<std::string, unsigned int>();
}

Shader::~Shader()
{
  glDeleteProgram(shaderID);
}

void Shader::Bind() const
{
  glUseProgram(shaderID);
}

void Shader::Unbind() const
{
  glUseProgram(0);
}

void Shader::CreateShader(ShaderTypes shaderType, std::string shaderPath)
{
  std::string shaderSource;
  if(ReadShaderFromFile(shaderPath, shaderSource) == -1)
    return;

  unsigned int compiledShader = CompileShader(shaderType, shaderSource.c_str());
  glAttachShader(shaderID, compiledShader);

  glLinkProgram(shaderID);
  glValidateProgram(shaderID);

  glDeleteShader(compiledShader);
}

void Shader::SetMat4(std::string uniformName, int count, bool transpose, glm::mat4 data)
{
  glUniformMatrix4fv(GetUniformLocation(uniformName), count, transpose, glm::value_ptr(data));
}

void Shader::SetVec3(std::string uniformName, glm::vec3 vec)
{
  glUniform3f(GetUniformLocation(uniformName), vec.x, vec.y, vec.z);
}

void Shader::SetVec3(std::string uniformName, float x, float y, float z)
{
  glUniform3f(GetUniformLocation(uniformName), x, y, z);
}

void Shader::SetFloat(std::string uniformName, float value)
{
  glUniform1f(GetUniformLocation(uniformName), value);
}

void Shader::SetInt(std::string uniformName, int value)
{
  glUniform1i(GetUniformLocation(uniformName), value);
}

unsigned int Shader::ReadShaderFromFile(std::string shaderPath, std::string &shaderSource)
{
  std::ifstream sourceFile;
  std::stringstream shaderStream;
  sourceFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try
  {
    sourceFile.open(shaderPath);
    shaderStream << sourceFile.rdbuf();
    sourceFile.close();
  }
  catch(std::ifstream::failure e)
  {
    std::cout << "ERROR: UNABLE TO READ SHADER FROM FILE: " << shaderPath << '\n';
    return -1;
  }
  shaderSource = shaderStream.str();
  return 0;
}

unsigned int Shader::CompileShader(ShaderTypes shaderType, std::string shaderSource)
{
  unsigned int id = glCreateShader(GetShaderType(shaderType));
  const char* charShaderSource = shaderSource.c_str();
  glShaderSource(id, 1, &charShaderSource, 0);
  glCompileShader(id);

  int result;
  glGetShaderiv(id, GL_COMPILE_STATUS, &result);

  if(!result)
  {
    int length;
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
    char* message = (char*)alloca(sizeof(char) * length);
    glGetShaderInfoLog(id, length, &length, message);

    std::cout << "ERROR Compiling shader: " << '\n';
    std::cout << message << '\n';

    glDeleteShader(id);
    return 0;
  }
  return id;
}

unsigned int Shader::GetUniformLocation(std::string uniformName)
{
  std::map<std::string, unsigned int>::iterator iter = uniformCache.find(uniformName);
  if (iter != uniformCache.end()) return iter->second;

  unsigned int uniformLocation = glGetUniformLocation(shaderID, uniformName.c_str());
  uniformCache.insert(std::pair<std::string, unsigned int>(uniformName, uniformLocation));

  if (uniformLocation == -1) 
    std::cout << "ERROR: The following Shader Uniform was not found: " << uniformName << '\n';

  return uniformLocation;
}

unsigned int Shader::GetShaderType(ShaderTypes shaderType)
{
  switch(shaderType)
  {
    case ShaderTypes::FRAGMENT: return GL_FRAGMENT_SHADER;
    case ShaderTypes::VERTEX: return GL_VERTEX_SHADER;
    default: return 0;
  }
}
