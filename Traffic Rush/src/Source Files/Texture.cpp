#include "Texture.h"
#include <GL/glew.h>
#include "stb_image.h"
#include <iostream>

Texture::Texture()
{
  textureIDs = std::vector<unsigned int>();
}

Texture::~Texture()
{
  for(auto textureID : textureIDs)
    glDeleteTextures(1, &textureID);

  textureIDs.clear();
}

unsigned int Texture::LoadTexture(const char* texturePath)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_2D, textureID);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int width, height, nrChannels;
  unsigned char* data = stbi_load(texturePath, &width, &height, &nrChannels, STBI_rgb_alpha);
  if (!data)
  {
    std::cout << "Failed to load texture: " << texturePath << '\n';
    return -1;
  }

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  stbi_image_free(data);

  textureIDs.push_back(textureID);
}

void Texture::Bind(unsigned int textureOffset) const
{
  glActiveTexture(GL_TEXTURE0 + textureOffset);
  glBindTexture(GL_TEXTURE_2D, textureIDs[textureOffset]);
}

void Texture::Unbind() const
{
  glBindTexture(GL_TEXTURE_2D, 0);
}
