#include "Game.h"
#include "Camera.h"
#include "GameObjects/Pikachu.h"
#include "GameObjects/Cube.h"
#include "GameObjects/TexturedCube.h"
#include "LightManager.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

int Game::WIDTH = 0;
int Game::HEIGHT = 0;

Game::Game()
{
	isRunning = false;
	window = nullptr;
	camera = nullptr;
	pikachu = nullptr;
	cube = nullptr;
	texturedCube = nullptr;
	lightManager = nullptr;
}

Game::~Game()
{
	isRunning = false;
	delete camera;
	delete pikachu;
	delete cube;
	delete texturedCube;
	delete lightManager;

	glfwDestroyWindow(window);
	glfwTerminate();
}

void Game::Initialise(unsigned int width, unsigned int height, const char* title)
{
	if(!glfwInit())
	{
		std::cout << "ERROR: Unable to initialise GLFW\n";
		return;
	}

	// Set OpenGL version to 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Use core profile instead of compatibility profile

	window = glfwCreateWindow(width, height, title, NULL, NULL);
	if(!window)
	{
		std::cout << "ERROR: Unable to create window\n";
		return;
	}
	glfwMakeContextCurrent(window);

	WIDTH = width;
	HEIGHT = height;

	if(glewInit() != GLEW_OK)
	{
		std::cout << "ERROR: Unable to initialise GLEW\n";
		return;
	}

	// Render closer fragments first
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// Enable backface culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// std::cout << "OpenGL version: " << glGetString(GL_VERSION) << '\n';
	isRunning = true;

	glm::vec3 cameraPosition = glm::vec3(0.0f, 0.0f, -10.0f);
	camera = new Camera(cameraPosition, 90.0f, 0.1f, 100.0f);

	pikachu = new Pikachu();
	cube = new Cube();
	texturedCube = new TexturedCube();

	lightManager = new LightManager();
	lightManager->DIRECTIONAL_LIGHT->direction = glm::vec3(0.0f);

	PointLight* pointLight = new PointLight();
	pointLight->position = glm::vec3(2, 2, -2);
	pointLight->ambient = glm::vec3(0.2f);
	pointLight->diffuse = glm::vec3(0.7f, 0.0f, 0.0f);
	pointLight->specular = glm::vec3(0.0f, 1.0f, 1.0f);
	pointLight->constant = 1.0f;
	pointLight->linear = 0.09f;
	pointLight->quadratic = 0.032f;

	lightManager->AddPointLight(pointLight);

	PointLight* newPointLight = new PointLight();
	newPointLight->position = glm::vec3(-2, -2, 2);
	newPointLight->ambient = glm::vec3(0.2f);
	newPointLight->diffuse = glm::vec3(0.0f, 0.0f, 1.0f);
	newPointLight->specular = glm::vec3(0.0f, 1.0f, 0.0f);
	newPointLight->constant = 1.0f;
	newPointLight->linear = 0.09f;
	newPointLight->quadratic = 0.032f;

	lightManager->AddPointLight(newPointLight);

	SpotLight* spotLight = new SpotLight();
	spotLight->position = glm::vec3(0, 0, -5);
	spotLight->direction = glm::vec3(0, 0, 1);
	spotLight->ambient = glm::vec3(0.0f);
	spotLight->diffuse = glm::vec3(1.0f);
	spotLight->specular = glm::vec3(0.0f, 1.0f, 0.0f);
	spotLight->innerRadius = glm::cos(glm::radians(12.0f));
	spotLight->innerRadius = glm::cos(glm::radians(15.0f));
	spotLight->constant = 1.0f;
	spotLight->linear = 0.09f;
	spotLight->quadratic = 0.032f;

	lightManager->AddSpotLight(spotLight);

	// This draws in wireframe mode (useful for debugging)
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void Game::Update()
{
	if(glfwWindowShouldClose(window)) isRunning = false;

	camera->Update();

	pikachu->Update();
	texturedCube->Update();
	cube->Update();
}

void Game::HandleInput()
{
	glfwPollEvents();
	// TODO: Add an Input class to handle input
}

void Game::Render()
{
	glClearColor(0.3f, 0.5f, 0.7f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// pikachu->Render();
	// cube->Render();
	texturedCube->Render();

	glfwSwapBuffers(window);
}